<?php

/**
 * @file
 * Plugin file for VBO Bottom  module
 */

/**
 * Implements hook_views_plugin().
 */
function vbo_bottom_views_plugins() {
  return array(
    'display_extender' => array(
      'vbo_bottom' => array(
        'title' => t('Hide VBO'),
        'help' => t('Hide the VBO if no data in view.'),
        'path' => drupal_get_path('module', 'vbo_bottom') . '/views',
        'handler' => 'vbo_bottom_plugin_display_extender',
      ),
    ),
  );
}
