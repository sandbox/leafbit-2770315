<?php

/**
 * @file
 * Views plugin handler. Contains all relevant options and related logic.
 * Implements the Views Form API.
 */
class vbo_bottom_plugin_display_extender extends views_plugin_display_extender {

  /**
   * Returns setting options.
   */
  function option_definition() {
    // Call parent method so that default functionality not override.
    $options = parent::option_definition();
    $options['vbo_bottom'] = array('default' => 'top');
    return $options;
  }

  /**
   * Provide the default form for setting options.
   */
  function options_form(&$form, &$form_state) {
    //Call parent method so that default functionality not override.
    parent::options_form($form, $form_state);
    $vbo_value = $this->display->get_option('vbo_bottom');
    switch ($form_state['section']) {
      case 'vbo_bottom':
        $form['#title'] .= t('VBO Actions position');
        $form['vbo_bottom'] = array(
          '#type' => 'select',
          '#options' => array('top' => t('Top'), 'bottom' => t('Bottom')),
          '#default_value' => isset($vbo_value) ? $this->display->get_option('vbo_bottom') : 'top',
          '#description' => t('Where to place the VBO actions?'),
        );
        break;
    }
  }

  /**
   * Perform any necessary changes to the form values prior to storage.
   * There is no need for this function to actually store the data.
   */
  function options_submit(&$form, &$form_state) {
    //Call parent method so that default functionality not override.
    parent::options_submit($form, $form_state);
    $vbo_bottom = isset($form_state['values']['vbo_bottom']) ? $form_state['values']['vbo_bottom'] : 'top';
    //If no value in any how for VBO bottom, then will store 0.
    switch ($form_state['section']) {
      case 'vbo_bottom':
        $this->display->set_option('vbo_bottom', $vbo_bottom);
        break;
    }
  }

  /**
   * Provide the summary for attachment options in the views UI.
   *
   * This output is returned as an array.
   */
  function options_summary(&$categories, &$options) {
    //Call parent method so that default functionality not override.
    parent::options_summary($categories, $options);
    $vbo_bottom = check_plain(trim($this->display->get_option('vbo_bottom')));
    $options['vbo_bottom'] = array(
      'category' => 'other',
      'title' => t('VBO actions position'),
      'value' => $vbo_bottom,
      'desc' => t('Change position of VBO actions.'),
    );
  }
}
